# quo automation: 
#  cd /home/lutzray/SyncQUO/Dev/AtomicSync/Sources/PostProduction/yaltc/yaltc
#  while inotifywait -e close_write tests/test_yaltc.py ; do python -m pytest; done
# mac:
#  cd /Volumes/secondaire/SyncQUO/Dev/AtomicSync/Sources/PostProduction/yaltc/yaltc
#  while true; do fswatch tests/test_yaltc.py | python -m pytest;done


from yaltc import Decoder, Recording
# import unittest
from pathlib import Path
from loguru import logger
import pytest
from pprint import pprint

# Subli Note: ctrl k j, k 1 

# memo: must run "python -m pytest"
# in /Volumes/secondaire/SyncQUO/Dev/AtomicSync/Sources/PostProduction/yaltc/yaltc
# or /home/lutzray/SyncQUO/Dev/AtomicSync/Sources/PostProduction/yaltc/yaltc

THIS_DIR = Path(__file__).parent
data_dir = THIS_DIR.parent / 'tests/data'

#
# Class Recording
#

def test_init_no_file():
    with pytest.raises(OSError):
        Recording('not_there.txt')

def test_init_attributes():
    rec = Recording(data_dir/'tasc220.wav')
    assert rec.AVpath == Path(data_dir/'tasc220.wav')
    assert type(rec.probe) == type({})
    assert rec.siblings == []

def test__get_multi_files_siblings():
    rec = Recording(data_dir/'4CH003I.wav')
    assert rec.siblings == ['4CH003M.wav']
    rec = Recording(data_dir/'4CH003M.wav')
    assert rec.siblings == ['4CH003I.wav']

def test_ffprobe_audio_stream():
    rec = Recording(data_dir/'tasc220.wav')
    stream = rec._ffprobe_audio_stream()
    assert stream['bit_rate'] == '1536000'
    assert stream['codec_type'] == 'audio'

def test_samplerate():
    rec = Recording(data_dir/'tasc220mono.wav')
    assert rec.samplerate() == 48000

def test_is_video_with_audio():
    assert not Recording(data_dir/'tasc220mono.wav').is_video_with_audio()
    assert Recording(data_dir/'DSC_7310.m4v').is_video_with_audio()

def test__load_sound_extract():
    pass
#
# Class Decoder
#

def test_read_sound_extract_find_YaLTC():
            # YaLTC_channel : int
            # which channel is sync track, 0 for left or mono, 
            # 1 for right. Value is None if not YaLTC found.

    rec = Recording(data_dir/'tasc220mono.wav')
    rec._read_sound_extract_find_YaLTC(0, 1.59)
    assert rec.YaLTC_channel == 0
    chisqr, center = rec.decoder._fit_trig_signal_to_convoluted_env()
    assert abs(center - 34594) < 10

#     rec = Recording(data_dir/'tasc220.wav')
#     rec._read_sound_extract_find_YaLTC(0, 1.59)
#     assert rec.decoder.YaLTC_channel == 0
#     rec = Recording(data_dir/'canon.wav')._read_sound_extract_find_YaLTC(0, 1.59)
#     assert rec.decoder.find_and_set_YaLTC_channel() == 0
#     rec = Recording(data_dir/'voicemono.wav')._read_sound_extract_find_YaLTC(0, 1.59)
#     assert rec.decoder.find_and_set_YaLTC_channel() == None

# def test_get_silent_zone_indices():
#     rec = Recording(data_dir/'canon.wav')._read_sound_extract_find_YaLTC(0, 1.59)
#     lft,rght = rec.decoder._get_silent_zone_indices()
#     assert lft == 16603 and rght == 33403
#     rec = Recording(data_dir/'canon.wav')._read_sound_extract_find_YaLTC(0.5, 1.59)
#     lft,rght = rec.decoder._get_silent_zone_indices()
#     assert lft == 0 and rght == 10674 # moved to the left

# @pytest.fixture
# def tasc220mono_rec():
#     return Recording(data_dir/'tasc220mono.wav')._read_sound_extract_find_YaLTC(0, 1.59)

# def test__get_envelope(tasc220mono_rec):
#     env = tasc220mono_rec.decoder._get_envelope()
#     assert abs(env.mean() - 0.5) < 0.1










# def test__get_silent_zone_indices(tasc220mono_rec):
#     n1, n2 = tasc220mono_rec.decoder._get_silent_zone_indices()
#     mean = (n1 + n2)/2
#     center = tasc220mono_rec.decoder.silent_zone_center
#     assert abs(mean - center) < 10
