from loguru import logger
import ffmpeg
import pathlib, os.path
import sys, scipy.signal, numpy as np
import matplotlib.pyplot as plt
import lmfit
# from lmfit import Parameters, fit_report, minimize
from numpy import arcsin, sin, pi
from matplotlib.lines import Line2D
import os, math
from datetime import datetime, timezone, timedelta
from collections import deque
try:
    from cfuzzyset import cFuzzySet as FuzzySet
except ImportError:
    from fuzzyset import FuzzySet
# pip install fuzzyset2


CACHING = True

SAFE_SILENCE_WINDOW_WIDTH = 350 # ms, not the full 500 ms, to accommodate decay
# used in _get_silent_zone_indices()
MINIMUM_LENGTH = 4 # sec
TRIAL_TIMES = [ # in seconds
            (0.5, -2),
            (0.5, -3.5),
            (2, -2),
            (2, -3.5)]
SOUND_EXTRACT_LENGTH = 1.56 # second
SYMBOL_LENGTH_TOLERANCE = 0.05 # relative
SAMD21_LATENCY = 63 # microseconds

################## pasted from FSKfreqCalculator.py output:
F1 = 630.00 # Hertz
F2 = 1190.00 # Hz , both from FSKfreqCalculator output
SYMBOL_LENGTH = 14.286 # ms, from FSKfreqCalculator.py
N_SYMBOLS_SAMD21 = 35 # including sync pulse
##################

def to_precision(x,p):
    """
    returns a string representation of x formatted with a precision of p

    Based on the webkit javascript implementation taken from here:
    https://code.google.com/p/webkit-mirror/source/browse/JavaScriptCore/kjs/number_object.cpp
    """
    x = float(x)
    if x == 0.:
        return "0." + "0"*(p-1)
    out = []
    if x < 0:
        out.append("-")
        x = -x
    e = int(math.log10(x))
    tens = math.pow(10, e - p + 1)
    n = math.floor(x/tens)
    if n < math.pow(10, p - 1):
        e = e -1
        tens = math.pow(10, e - p+1)
        n = math.floor(x / tens)
    if abs((n + 1.) * tens - x) <= abs(n * tens -x):
        n = n + 1
    if n >= math.pow(10,p):
        n = n / 10.
        e = e + 1
    m = "%.*g" % (p, n)
    if e < -2 or e >= p:
        out.append(m[0])
        if p > 1:
            out.append(".")
            out.extend(m[1:p])
        out.append('e')
        if e > 0:
            out.append("+")
        out.append(str(e))
    elif e == (p -1):
        out.append(m)
    elif e >= 0:
        out.append(m[:e+1])
        if e+1 < len(m):
            out.append(".")
            out.extend(m[e+1:])
    else:
        out.append("0.")
        out.extend(["0"]*-(e+1))
        out.append(m)

    return "".join(out)

class Decoder:
    """
    Object encapsulating DSP processes to demodulate YaLTC track from audio file;
    Decoders are instantiated by their respective Recording object. Produces
    plots on demand for diagnostic purposes.

    Attributes:
        sound_extract : numpy.ndarray of int16, shaped (N) 
            sound data extract, could be anywhere in the audio file (start, end, etc...)
            Set by Recording object. This audio signal might or might not be a YaLTC track.
        sound_extract_position : int
            where the sound_extract is located in the file, samples
        samplerate : int
            sound sample rate, set by Recording object.
        cached_convolution_fit : dict
            if _fit_trig_signal_to_convoluted_env() has already been called, will use cached
            values if sound_extract_position is the same. 

    """

    def __init__(self):
        """
        Initialises Decoder

        Returns
        -------
        an instance of Decoder.

        """
        self.sound_data_extract = None
        self.cached_convolution_fit = {'sound_extract_position': None}

    def set_sound_extract_and_sr(self, extract, s_r, where):
        self.sound_extract = extract
        self.samplerate = s_r
        self.sound_extract_position = where
        logger.debug('sound_extract set, samplerate %i location %i'%(s_r, where))
   

        # there's always at least one complete 0.5 silence interval in a 1.5 second signal

    def _get_envelope(self):
        """
        Compute self.sound_extract envelope, filtering its hilbert transform.
        Uses scipy.signal.hilbert() and scipy.signal.savgol_filter();
        window_length and polyorder savgol_filter() parameters values 
        have been found by hit and miss on audio data.
        Values are roughly normalized: between 0 and approximately 1.0

        Returns
        -------
        numpy.ndarray the same length of self.recording.sound_extract

        """
        WINDOW_LENGTH, POLYORDER = (15, 3) # parameters found by experiment, hit and miss
        abs_hil = np.abs(scipy.signal.hilbert(self.sound_extract))
        envelope = scipy.signal.savgol_filter(abs_hil,
                                              WINDOW_LENGTH, POLYORDER)
        mean = envelope.mean()
        factor = 0.5/mean # since 50% duty cycle
        return factor*envelope

    def _get_signal_level(self):
        abs_signal = abs(self.sound_extract)
        return 2 * abs_signal.mean() # since 50% duty cycle

    def _get_square_convolution(self):
        """
        Compute self.sound_extract envelope convolution with a square signal of
        0.5 second width, using numpy.convolve().
        Values are roughly normalized: between 0 and approximately 1.0


        Returns in a tuple
        -------
    
         #1 the normalized convolution, a numpy.ndarray shorter than 
             self.sound_extract, see numpy.convolve(..., mode='valid')

         #2 a list of int, samples indexes where the convolution is computed

        """
        sqr_window_width = int(0.5*self.samplerate) # in samples
        sqr_signal = np.ones(sqr_window_width,dtype=int)
        envelope = self._get_envelope()
        convol = np.convolve(envelope, sqr_signal,
                             mode='valid')/sqr_window_width
        start = int(sqr_window_width/2)
        x = range(start, len(convol) + start)
        return [*x], convol
    
    def _fit_trig_signal_to_convoluted_env(self):
        """
        Try to fit a triangular signal to the envelope convoluted with a square
        signal to evaluate if audio is composed of 0.5 sec signal and 0.5 s 
        silence. If so, the convolution is a triangular fct and a
        Levenberg–Marquardt fit is used to detect this occurence (lmfit).
        Results are cached in self.cached_convolution_fit alongside
        self.sound_extract_position for hit/miss checks.


        Returns
        -------
        float
            chi_sqr from lmfit.minimize(), indicative of fit quality
        int
            position of triangular minimum (base of the v shape), this
            corresponds to the center of silent zone.
        """
        if CACHING and self.sound_extract_position == \
            self.cached_convolution_fit['sound_extract_position']:
            logger.debug('cache hit')
            return (
                self.cached_convolution_fit['chi_square'],
                self.cached_convolution_fit['minimum position'])
                # cached!
        x_shifted, convolution = self._get_square_convolution()
        shift = x_shifted[0] # convolution is shorter than sound envelope
        # see numpy.convolve(..., mode='valid')
        x = np.arange(len(convolution))
        trig_params = lmfit.Parameters()
        trig_params.add(
            'A', value=1, min=0, max=2
            )
        period0 = 2*self.samplerate
        trig_params.add(
            'period', value=period0, min=0.9*period0,
            max=1.1*period0
            )
        trig_params.add(
            'min_position', value=len(convolution)/2
            ) # at center
        def trig_wave(pars, x, signal_data=None):
            # looking for phase sx with a sin of 1 sec period and 0<y<1.0
            A = pars['A']
            p = pars['period']
            mp = pars['min_position']
            model = 2*A*arcsin(abs(sin((x - mp)*2*pi/p)))/pi
            if signal_data is None:
                return model
            return model - signal_data
        fit_trig = lmfit.minimize(
                            trig_wave, trig_params,
                            args=(x,), kws={'signal_data': convolution}
                            )
        chi_square = fit_trig.chisqr
        min_position = int(fit_trig.params['min_position'].value)
        logger.debug('chi_square %.1f minimum convolution position %i'%
                     (chi_square, min_position + shift))
        self.cached_convolution_fit['sound_extract_position'] = self.sound_extract_position
        self.cached_convolution_fit['chi_square'] = chi_square
        self.cached_convolution_fit['minimum position'] = min_position + shift
        return chi_square, min_position + shift

    def extract_seems_YaLTC(self):
        """
        evaluate if sound data is half signal, half silence
        no test is done on frequency components nor BFSK modulation.

        Returns
        -------
        True if sound seems YaLTC

        """
        chi_square, _ = self._fit_trig_signal_to_convoluted_env()
        seems_YaLTC = chi_square < 200 # good fit so, yes
        logger.debug('seems YaLTC: %s'%seems_YaLTC)
        return seems_YaLTC

    def _get_silent_zone_indices(self):
        """
        Returns silent zone boundary positions relative to the start
        of self.sound_extract. Adjustement are made so a complete
        0.5 second signal is at the left of the silent zone for word
        decoding.

        Returns
        -------
        left_window_boundary : int
            left indice.
        right_window_boundary : int
            right indice.

        """
        _, silence_center_position = self._fit_trig_signal_to_convoluted_env()
        srate = self.samplerate
        half_window = int(SAFE_SILENCE_WINDOW_WIDTH * 1e-3 * srate/2)
        left_window_boundary = silence_center_position - half_window
        right_window_boundary = silence_center_position + half_window
        margin = 0.75 * srate
        logger.debug('initial silent zone, left: %i, right %i, center %i'%
            (left_window_boundary, right_window_boundary,
             silence_center_position))
        no_word_on_the_right = \
            len(self.sound_extract) - silence_center_position <  margin
        if no_word_on_the_right:
            logger.debug('shifting silence left: no complete word on the right')
            right_window_boundary -= srate
            left_window_boundary -= srate
            if left_window_boundary < 0 :
                left_window_boundary = 0
        logger.debug('final silent zone, left: %i, right %i'%
            (left_window_boundary, right_window_boundary))
        return left_window_boundary, right_window_boundary
 
    def _get_silence_floor(self):
        start_silent_zone, end_silent_zone = self._get_silent_zone_indices()
        signal = self.sound_extract
        silent_signal = signal[start_silent_zone:end_silent_zone]
        max_value = 1.001*np.abs(silent_signal).max() # a little headroom
        five_sigmas = 5 * silent_signal.std()
        return max(max_value, five_sigmas) # if guassian, five sigmas will do it

    def make_silence_analysis_plot(self, title=None, filename=None):
        start_silent_zone, end_silent_zone = self._get_silent_zone_indices()
        signal = self.sound_extract
        x_signal = range(len(signal))
        x_convolution, convolution = self._get_square_convolution()
        scaled_convo = self._get_signal_level()*convolution
        # since 0 < convolution < 1
        max_noise = self._get_silence_floor()
        fig, ax = plt.subplots()
        yt = ax.get_yaxis_transform()
        ax.hlines(
            max_noise, 0, 1, 
            transform=yt, linewidth=1, colors='blue'
            )
        ax.hlines(
            0, 0, 1, 
            transform=yt, linewidth=0.5, colors='black'
            )
        plt.title('file %s'%title)
        custom_lines = [
            Line2D([0], [0], color='blue', lw=2),
            Line2D([0], [0], color='black', lw=2),
            Line2D([0], [0], color='green', lw=2),
            ]
        ax.legend(
            custom_lines,
            'silence threshold,sqr convolution,silence zone'.split(','),
            loc='lower right')
        plt.ylim( (pow(10,0),pow(10,5)) )
        ax.semilogy(
            x_signal, signal,
            marker='o', markersize='1',
            linewidth=0.3, color='purple', alpha=0.3)
        ax.axvspan(
            start_silent_zone, end_silent_zone,
            alpha=0.1, color='green')
        ax.semilogy(
            x_convolution, scaled_convo,
            marker='o', markersize='0.2',
            linewidth=0.3, color='black', alpha=0.3)
        ax.set_xlabel('Decoder.sound_extract samples')
        if filename == None:
            plt.show()
        else:
            logger.debug('saving ilence_analysis_plot to %s'%filename)
            plt.savefig(
                filename,
                format="png")
            plt.close()

    def make_slicing_plot(self, title=None, filename=None):
        signal = self.sound_extract
        start = self.sound_extract_position
        x_signal_in_file = range(
                                start,
                                start + len(signal)
                                )
        start_silent_zone, end_silent_zone = self._get_silent_zone_indices()
        sync_pulse = self._detect_sync_pulse_position()
        wwp = self._get_word_width_parameters(sync_pulse)
        search_start_position = wwp['search_start_position'] + start
        search_end_position = wwp['search_end_position'] + start
        fig, ax = plt.subplots()
        plt.title('bit slicing for\n%s'%title)
        ax.plot(
            x_signal_in_file, signal,
            marker='.', markersize='1',
            linewidth=0.3, color='purple', alpha=0.3)
        yt = ax.get_yaxis_transform()
        ax.hlines(
            wwp['word_width_threshold'], 0, 1, 
            transform=yt, linewidth=0.6, colors='blue')
        ax.hlines(
            0, 0, 1, 
            transform=yt, linewidth=0.6, colors='black')
        ax.hlines(
            -wwp['word_width_threshold'], 0, 1, 
            transform=yt, linewidth=0.6, colors='blue')
        xt = ax.get_xaxis_transform()
        ax.vlines(
            search_start_position,
            0, 1, transform=xt, linewidth=0.6, colors='blue')
        ax.vlines(
            search_end_position,
            0, 1, transform=xt, linewidth=0.6, colors='blue')
        ax.plot(
            [sync_pulse + start], [0],
            marker='D', markersize='7',
            linewidth=0.3, color='blue', alpha=0.3)
        ax.plot(
            [start_silent_zone + start], [0],
            marker='>', markersize='10',
            linewidth=0.3, color='green', alpha=0.3)
        ax.plot(
            [end_silent_zone + start], [0],
            marker='<', markersize='10',
            linewidth=0.3, color='green', alpha=0.3)
        symbols_indices = self._get_BFSK_symbols_boundaries(sync_pulse)
        word_lft, word_rght = self._get_BFSK_word_boundaries(sync_pulse)
        ax.vlines(
            word_lft + start, 0, 1,
            transform=ax.get_xaxis_transform(),
            linewidth=0.6, colors='red')
        ax.vlines(
            word_rght + start, 0, 1, 
            transform=ax.get_xaxis_transform(),
            linewidth=0.6, colors='red')
        for x in symbols_indices + self.sound_extract_position:
            ax.vlines(
            x, 0, 1,
            transform=ax.get_xaxis_transform(),
            linewidth=0.3, colors='green')
        ax.set_xlabel(
            'samples in file')
        plt.xlim(
            [sync_pulse - 300 + start, wwp['search_end_position'] + 400 + start])
        if filename == None:
            plt.show()
        else:
            plt.ylim(
                [-1.5*wwp['word_width_threshold'],
                1.1*signal.max()])
            height = 1000
            plt.savefig(
                filename,
                format='png',
                dpi=height/fig.get_size_inches()[1])
            plt.close()

    def _detect_sync_pulse_position(self):
        start_silent_zone, end_silent_zone = self._get_silent_zone_indices()
        noise_threshold = self._get_silence_floor()
        abs_signal = abs(self.sound_extract)
        abs_signal_after_silence = abs_signal[end_silent_zone:]
        first_point = \
            np.argmax(abs_signal_after_silence > noise_threshold)
        return (first_point + end_silent_zone)

    def _get_word_width_parameters(self, pulse_position):
        abs_signal = abs(self.sound_extract)
        half_amplitude = abs_signal.mean() # since 50% duty cycle
        params = {'word_width_threshold':1.3*half_amplitude}
        sr = self.samplerate
        presumed_symbol_length = SYMBOL_LENGTH*1e-3*sr
        # in ms, now in samples
        params['search_start_position'] = \
            int(0.67 * presumed_symbol_length + pulse_position)
        presumed_word_width = presumed_symbol_length * (N_SYMBOLS_SAMD21 - 1)
        params['search_end_position'] = \
                    int(params['search_start_position'] + \
                    presumed_word_width) + 1700 # samples for headroom
        params['presumed_symbol_length'] = presumed_symbol_length
        return params

    def _get_BFSK_word_boundaries(self, pulse_position):
        n_bits = N_SYMBOLS_SAMD21 - 1
        sr = self.samplerate
        wwp = self._get_word_width_parameters(pulse_position)
        search_start_position = wwp['search_start_position']
        search_end_position = wwp['search_end_position']
        word_width_threshold = wwp['word_width_threshold']
        word_extract = self.sound_extract[search_start_position :
            search_end_position]
        logger.debug('word boundaries search zone: %i and %i'%\
            (search_start_position, search_end_position))
        logger.debug(' in extract at %i in file'%
                self.sound_extract_position)
        flipped_extract = np.flip(np.abs(word_extract))
        right_boundary = len(word_extract) - \
            np.argmax(flipped_extract > word_width_threshold)  + \
                search_start_position
        left_boundary = np.argmax(
            np.abs(word_extract) > word_width_threshold)  + \
            search_start_position
        left_in_file = left_boundary + self.sound_extract_position
        right_in_file = right_boundary + self.sound_extract_position
        logger.debug('word boundaries: %i and %i in file'%
                                (left_in_file, right_in_file))
        eff_symbol_length = 1e3*(right_boundary-left_boundary)/(n_bits*sr)
        logger.debug('effective symbol length %.4f ms '%
                                eff_symbol_length)
        logger.debug('presumed symbol length %.4f ms '%SYMBOL_LENGTH)
        relative_error = (eff_symbol_length-SYMBOL_LENGTH)/SYMBOL_LENGTH
        if relative_error > SYMBOL_LENGTH_TOLERANCE:
            logger.error(
                'actual symbol length differs too much: %.2f vs %.2f ms, aborting'%
                (eff_symbol_length, SYMBOL_LENGTH))
            return None, None
        logger.debug(' relative discrepancy %.4f%%'%(abs(100*relative_error)))
        return left_boundary, right_boundary

    def _get_BFSK_symbols_boundaries(self, pulse_position):
        left_boundary, right_boundary = \
                self._get_BFSK_word_boundaries(pulse_position)
        if left_boundary is None:
            return None
        symbol_width_samples = \
                (right_boundary - left_boundary)/(N_SYMBOLS_SAMD21 - 1)
        symbol_positions = symbol_width_samples * \
                np.arange(0, N_SYMBOLS_SAMD21 + 1) + \
                pulse_position
        int_symb_positions = symbol_positions.round().astype(int)
        logger.debug('symbol positions %s samples in file'%
                (int_symb_positions + self.sound_extract_position))
        return int_symb_positions

    def _values_from_bits(self, bits):
        word_payload_bits_positions = {
            'version':(0,2),
            'clock source':(2,3),
            'seconds':(3,9),
            'minutes':(9,15),
            'hours':(15,20),
            'day':(20,25),
            'month':(25,29),
            'year offset':(29,34),
            }
        binary_words = { key : bits[slice(*value)]
                for key, value 
                in word_payload_bits_positions.items()
                }
        int_values = { key : self._get_int_from_binary_str(val)
                for key, val                 in binary_words.items()
                }
        logger.debug(' Demodulated values %s'%int_values)
        return int_values

    def _slice_sound_extract(self, symbols_indices):
        indices_left_shifted = deque(list(symbols_indices))
        indices_left_shifted.rotate(-1)
        all_intervals = list(zip(
                                symbols_indices,
                                indices_left_shifted
                                ))
        word_intervals = all_intervals[1:-1]
        # [0, 11, 23, 31, 45] => [(11, 23), (23, 31), (31, 45)]
        logger.debug('slicing intervals, word_intervals = %s'%
                                                word_intervals)
        # skip sample after pulse, start at BFSK word
        slices = [self.sound_extract[slice(*pair)]
                            for pair in word_intervals]
        np.set_printoptions(threshold=5)
        # logger.debug('data slices: \n%s'%pprint.pformat(slices))
        return slices

    def _get_main_frequency(self, symbol_data):
        w = np.fft.fft(symbol_data)
        freqs = np.fft.fftfreq(len(w))
        idx = np.argmax(np.abs(w))
        freq = freqs[idx]
        freq_in_hertz = abs(freq * self.samplerate)
        return int(round(freq_in_hertz))

    def _get_bit_from_freq(self, freq):
        if math.isclose(freq, F1, rel_tol=0.05):
            return '0'
        if math.isclose(freq, F2, rel_tol=0.05):
            return '1'
        else:
            return None

    def _get_int_from_binary_str(self, string_of_01s):
        return int(''.join(reversed(string_of_01s)),2)
        # LSB is leftmost in YaLTC

    def _demod_values_are_OK(self, values_dict):
        ranges = {
            'seconds': range(60),
            'minutes': range(60),
            'hours': range(24),
            'day': range(1,32),
            'month': range(1,13),
            }
        for key in ranges:
            val = values_dict[key]
            ok =  val in ranges[key]
            logger.debug(
                'checking range for %s: %i, Ok? %s'%(key, val, ok))
            if not ok:
                logger.error('demodulated value is out of range')
                return False
        return True

    def get_time_in_sound_extract(self):
        if self.sound_extract is None:
            return None
        start_silent_zone, end_silent_zone = self._get_silent_zone_indices()
        pulse_position = self._detect_sync_pulse_position()
        logger.debug('found sync pulse at %i in extract, at %i in file'%
            (pulse_position, pulse_position + self.sound_extract_position))
        symbols_indices = self._get_BFSK_symbols_boundaries(pulse_position)
        if symbols_indices is None:
            return None
        sliced_data = self._slice_sound_extract(symbols_indices)
        frequencies = [self._get_main_frequency(data_slice)
                            for data_slice
                            in sliced_data
                            ]
        logger.debug('frequencies = %s'%frequencies)
        bits = [self._get_bit_from_freq(f) for f in frequencies]
        for i, bit in enumerate(bits):
            if bit == None:
                logger.warning('cant decode frequency %i for bit at %i-%i'%(
                                frequencies[i],
                                symbols_indices[i],
                                symbols_indices[i+1]))
        if None in bits:
            return None
        bits_string = ''.join(bits)
        logger.debug('bits = %s'%bits_string)
        time_values = self._values_from_bits(bits_string)
        time_values['pulse at'] = (pulse_position +
                                    self.sound_extract_position -
                                    SAMD21_LATENCY*1e-6*self.samplerate)
        time_values['clock source'] = 'GPS' \
            if time_values['clock source'] == 1 else 'RTC'
        if self._demod_values_are_OK(time_values):
            return time_values
        else:
            return None

class Recording:
    """
    Wrapper for file objects. Read operations, ffmpeg and fprobe functions
    
    Attributes:
        AVpath : a pathlib.path
            AVpath of video+sound file
        probe : dict
            returned value of ffmpeg.probe(self.AVpath)
        YaLTC_channel : int
            which channel is sync track, 0 for left or mono, 
            1 for right. Value is None if not YaLTC found.
            Set in _read_sound_extract().
        decoder : yaltc.decoder
            associated decoder object, if file is audiovideo
        siblings: list of filenames
            if this is not a multi-file recording, this list is empty
            (doesnt includes own name).
        true_samplerate: str
            true sample rate using GPS time
        start_time: datetime or str
            time and date of the first sample in the file, cached
            after a call to get_start_time(). Value on initialization
            is None, 'Failed' if previous get_start_time() found none.

    """

    def __init__(self, filename):
        """
        Set filename string and check if file exists, does not read
        any media data right away but uses ffprobe to parses the file and
        sets probe attribute. 
        Logs a warning if ffprobe cant interpret the file or if file
        has no audio; if file contains audio, instantiates a Decoder object

        Parameters
        ----------
        filename : string
            string AVpath of the sound file

        Raises
        ------
        an Exception if filename doesnt exist

        """
        self.true_samplerate = None
        self.start_time = None
        self.decoder = None
        self.siblings = None
        self.probe = None
        self.YaLTC_channel = None
        self.AVpath = pathlib.Path(filename)
        logger.debug('Initializing Recording object:')
        logger.debug(' file %s'%self.AVpath.name)
        logger.debug(' in directory %s'%self.AVpath.parent)
        recording_init_fail = ''
        if not self.AVpath.is_file():
            raise OSError('file "%s" doesnt exist'%self.AVpath)        
        try:
            self.probe = ffmpeg.probe(self.AVpath)
        except:
            logger.warning('"%s" is not recognized by ffprobe'%self.AVpath)
            recording_init_fail = 'not recognized by ffprobe'
        if self.probe is None:
            recording_init_fail ='no ffprobe'
        elif self.probe['format']['probe_score'] < 99:
            logger.warning('ffprobe score too low')
            # raise Exception('ffprobe_score too low: %i'%probe_score)
            recording_init_fail = 'ffprobe score too low'
        elif not self.has_audio():
            # logger.warning('file has no audio')
            recording_init_fail = 'no audio in file'
        elif self.get_duration() < MINIMUM_LENGTH:
            recording_init_fail = 'file too short, %f s'%self.get_duration()
        if recording_init_fail is '':
            self.decoder = Decoder()
            self._set_multi_files_siblings()
        else:
            logger.warning('Recording init failed: %s'%recording_init_fail)
            self.probe = None
            self.decoder = None
            self.siblings = None
        logger.debug('ffprobe found: %s'%self.probe)

    def _find_time_around(self, time):
        if time < 0:
            there = self.get_duration() + time
        else:
            there = time
        self._read_sound_extract_find_YaLTC(there, SOUND_EXTRACT_LENGTH)
        if self.YaLTC_channel is None:
            return None
        else:
            return self.decoder.get_time_in_sound_extract()

    def _get_timedate_from_dict(self, time_dict):
        python_datetime = datetime(
            time_dict['year offset'] + 2021,
            time_dict['month'],
            time_dict['day'],
            time_dict['hours'],
            time_dict['minutes'],
            time_dict['seconds'],
            tzinfo=timezone.utc)
        return python_datetime

    def _two_times_are_coherent(self, t1, t2):
        """
        For error checking. This verifies if two sync pulse apart
        are correctly space with sample interval deduced from
        time difference of demodulated YaLTC times. The same 
        process is used for determining the true sample rate
        in _compute_true_samplerate(). On entry check if either time
        is None, return False if so.

        Parameters
        ----------
        t1 : dict of demodulated time
            see Decoder.get_time_in_sound_extract().
        t2 : dict of demodulated time
            see Decoder.get_time_in_sound_extract().

        Returns
        -------
        TYPE
            DESCRIPTION.

        """
        if t1 == None or t2 == None:
            return False
        datetime_1 = self._get_timedate_from_dict(t1)
        datetime_2 = self._get_timedate_from_dict(t2)
        sample_position_1 = t1['pulse at']
        sample_position_2 = t2['pulse at']
        samplerate = self.get_samplerate()
        delta_seconds_with_samples = \
                (sample_position_2 - sample_position_1)/samplerate
        delta_seconds_with_UTC = (datetime_2 - datetime_1).total_seconds()
        logger.debug('check for delay between \n%s and\n%s'%
                            (datetime_1, datetime_2))
        logger.debug('delay using samples number: %f sec'%
                            (delta_seconds_with_samples))
        logger.debug('delay using timedeltas: %.2f sec'%
                            (delta_seconds_with_UTC))
        return round(delta_seconds_with_samples) == delta_seconds_with_UTC

    def _compute_true_samplerate(self, t1, t2):
        datetime_1 = self._get_timedate_from_dict(t1)
        pulse_position_1 = t1['pulse at']
        datetime_2 = self._get_timedate_from_dict(t2)
        pulse_position_2 = t2['pulse at']
        delta_seconds_whole = (datetime_2 - datetime_1).total_seconds()
        delta_samples_whole = pulse_position_2 - pulse_position_1
        true_samplerate = delta_samples_whole / delta_seconds_whole
        logger.debug('delta seconds between pulses %f'%
                                delta_seconds_whole)
        logger.debug('delta samples between pulse %i'%
                                delta_samples_whole)
        logger.debug('true sample rate = %s Hz'%
                                to_precision(true_samplerate, 8))
        return true_samplerate

    def get_start_time(self):
        if self.start_time is not None:
            return self.start_time
        cached_times = {}
        def find_time(t_sec):
            time_k = int(t_sec)
            # if cached_times.has_key(time_k):
            if CACHING and time_k in cached_times:
                logger.debug('cache hit _find_time_around() for t=%s s'%time_k)
                return cached_times[time_k]
            else:
                logger.debug('_find_time_around() for t=%s s not cached'%time_k)
                new_t = self._find_time_around(t_sec)
                cached_times[time_k] = new_t
                return new_t
        for i, pair in enumerate(TRIAL_TIMES):
            near_beg, near_end = pair
            logger.debug('Will try to decode times at: %s and %s secs'%
                (near_beg, near_end))
            logger.debug('Trial #%i, beg at %f'%(i+1, near_beg))
            if i > 1:
                logger.warning('More than one trial: #%i/4'%i)
            # time_around_beginning = self._find_time_around(near_beg)
            time_around_beginning = find_time(near_beg)
            if self.YaLTC_channel is None:
                return None
            logger.debug('Trial #%i, end at %f'%(i+1, near_end))
            # time_around_end = self._find_time_around(near_end)
            time_around_end = find_time(near_end)
            logger.debug('trial result: \n%s\n%s'%
                    (time_around_beginning, time_around_end))
            if self._two_times_are_coherent(
                    time_around_beginning,
                    time_around_end):
                break
        if None in [time_around_beginning, time_around_end]:
            logger.warning('didnt find two times in file')
            self.start_time = 'Failed'
            return None
        true_sr = self._compute_true_samplerate(
                        time_around_beginning,
                        time_around_end)
        self.true_samplerate = to_precision(true_sr,8)
        first_pulse_position = time_around_beginning['pulse at']
        delay_from_start = timedelta(
                seconds=first_pulse_position/true_sr)
        first_time_date = self._get_timedate_from_dict(
                                    time_around_beginning)
        start_UTC = first_time_date - delay_from_start
        logger.debug('recording started at %s'%start_UTC)
        self.start_time = start_UTC
        return start_UTC

  

    def _read_sound_extract_find_YaLTC(self, time_where, chunk_length):
        """
        Loads audio data reading from self.AVpath;
        Split data into channels if stereo;
        Send this data to Decoder object with set_sound_extract_and_sr() to find
        which channel contains a YaLTC track and sets YaLTC_channel
        accordingly. On exit, Decoder.sound_extract
        contains YaLTC data ready to be demodulated. If not, Decoder.YaLTC_channel
        is set to None.

        Parameters
        ----------
        time_where : float
            time of the audio chunk start, in seconds.
        chunk_length : float
            length of the audio chunk, in seconds.

        Returns
        -------
        this Recording instance

        """
        decoder = self.decoder
        if not self.has_audio():
            self.YaLTC_channel = None
            return
        logger.debug('will read around %.2f sec'%time_where)
        dryrun = (ffmpeg
            .input(str(self.AVpath), ss=time_where, t=chunk_length)
            .output('pipe:', format='s16le', acodec='pcm_s16le')
            .get_args())
        dryrun = ' '.join(dryrun)
        logger.debug('using ffmpeg-python built args to pipe wav file into numpy array:\nffmpeg %s'%dryrun)
        try:
            out, _ = (ffmpeg
                .input(str(self.AVpath), ss=time_where, t=chunk_length)
                .output('pipe:', format='s16le', acodec='pcm_s16le')
                .global_args("-loglevel", "quiet")
                .global_args("-nostats")
                .global_args("-hide_banner")
                .run(capture_stdout=True))
            data = np.frombuffer(out, np.int16)
        except ffmpeg.Error as e:
            print('error',e.stderr)        
        audio_stream = self._ffprobe_audio_stream()
        nb_of_audio_channel = audio_stream['channels']
        sound_extract_position = int(self.get_samplerate()*time_where) # from sec to samples
        if nb_of_audio_channel == 1:
            logger.debug('file is mono')
            audio_data = data
            decoder.set_sound_extract_and_sr(audio_data, self.get_samplerate(),
                sound_extract_position)
            if decoder.extract_seems_YaLTC():
                self.YaLTC_channel = 0
                return
            else:
                logger.warning('mono file should be YaLTC and it isnt')
                decoder.set_sound_extract_and_sr(None, self.get_samplerate(),
                    sound_extract_position)
                self.YaLTC_channel = None
                return
        if nb_of_audio_channel == 2:
            # stereo data is interleaved, so this dis-interleaved it:
            left_audio_data, right_audio_data = data.reshape(int(len(data)/2),2).T
            # try with left channel
            decoder.set_sound_extract_and_sr(left_audio_data, self.get_samplerate(),
                sound_extract_position)
            if decoder.extract_seems_YaLTC():
                self.YaLTC_channel = 0
                logger.debug('find YaLTC channel is left')
                return
            # try with right channel
            decoder.set_sound_extract_and_sr(right_audio_data, self.get_samplerate(),
                sound_extract_position)
            if decoder.extract_seems_YaLTC():
                self.YaLTC_channel = 1
                logger.debug('find YaLTC channel is right')
                return
            else:
                self.YaLTC_channel = None
                logger.warning('found no YaLTC channel')
        return self
    
    def seems_to_have_YaLTC_at_beginning(self):
        if self.probe is None:
            return False
        self._read_sound_extract_find_YaLTC(TRIAL_TIMES[0][0], SOUND_EXTRACT_LENGTH)
        return self.YaLTC_channel is not None



