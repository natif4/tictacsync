# yaltc

## Warning: this is at pre-alpha stage

Unfinished crappy code ahead, but should run without errors. Some functionalities are still missing. Don't run the code without parental supervision.

## Description

`yaltc` is a python module to sync audio and video files shot
with [dual system sound](https://www.learnlightandsound.com/blog/2017/2/23/how-to-record-sound-for-video-dual-systemsync-sound)  using a specific hardware timecode generator
called [Tic Tac Sync](https://hackaday.io/project/176196-atomic-synchronator). The timecode is called YaLTC for *yet
another longitudinal time code* and should be recorded on a scratch
track on each device for the syncing to be performed.

## Installation

    git clone git@git.sr.ht:~proflutz/yaltc

## Dependencies

Not obvious:

    pip install ffmpeg-python


## Usage

To kick the tires with data read from `tests/data`, from the cloned git directory, type

    python yaltc.py tests/data/tasc220.wav

or for a very verbose output:

    python yaltc.py -v tests/data/tasc220.wav


Surely the python interpreter will complain about other missing modules (scipy, soundfile, numpy, etc...) and you'll need a running [ffmpeg](https://ffmpeg.org) installation on your PATH.


To run some tests

    python -m pytest


