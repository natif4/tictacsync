#!/usr/bin/env python3

import sys
from software.gui import Gui

def main():
    """
    Main function of the application, called the other function to make the app run properly
    """
    use_gui()


def use_gui():
    """
    Initialize the GUI and apply some change on it
    """
    gui = Gui()
    gui.mainloop()

if __name__ == "__main__":
    main()
    sys.path.append('../')
