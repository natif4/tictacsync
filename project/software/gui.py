#!/usr/bin/env python3

from tkinter import *
from tkinter import filedialog as fd
from tkinter import messagebox
from tkinter.ttk import Progressbar
from script.script import verify_file
from script.media import Media

BG_COLOR = '#212121'
BG_COLOR_LIGHT = '#484848'
BG_COLOR_DARK = '#000000'

ACCENT_COLOR = '#29b6f6'
ACCENT_COLOR_LIGHT = '#73e8ff'
ACCENT_COLOR_DARK = '#0086c3'

TEXT_COLOR = '#ffffff'
TEXT_COLOR_ACCENT = '#000000' 

HEADLINE_FONT = ('Ubuntu', 20, "bold")
INPUT_FONT = ('Ubuntu', 12)

FILE_TYPES = [('Video files','.mp4 .avi .mpg .m4v'),('Audio files','.wav .mp3 .audio'), ('All files', '*.*')]

class Gui(Tk):
    '''
    The GUI class, its use primarly to generate a window and its content.
    Its also responsible for user interaction.

    Args:
        Tk (TK): The main window of the application, commonly called 'root'
    '''
    def __init__(self):
        """
        Constructor for the Windows, assign all the window property and also call the other function to build the window
        """
        super().__init__()
        self.title("TicTacSync")
        self.config(bg=BG_COLOR)
        self.resizable(False, False)
        self.image = PhotoImage().blank()
        self.initialize_variables()
        self.initialize_widget()
        self.place_widget()
        
    def initialize_variables(self):
        """
        Initialize the variable to false. Use for the checkbox
        """
        self.labelText = StringVar(self)
        self.monoText = StringVar(self,"")
        self.stereoText = StringVar(self,"")
        self.onlySoundText = StringVar(self,"")
        self.hasVideoText = StringVar(self,"")
        
    def initialize_widget(self):
        '''
        Initialize all the widget use in the application.
        '''
        self.initialize_frame()
        self.initialize_label()
        self.initialize_button()
        self.initialize_misc()
        self.initialize_checkbox()

    def initialize_frame(self):
        """
        Initialize all the Frame widget.
        Set the dimension, the colors, and the type of widget.
        """
        self.left_frame = Frame(self, width=300, height=600, bg=BG_COLOR_LIGHT)
        self.right_frame = Frame(self, width=500, height=600, bg=BG_COLOR_LIGHT)
        self.spacer_top = Frame(self.left_frame, width=300, height=30, bg=BG_COLOR_LIGHT)
        self.spacer_middle = Frame(self.left_frame, width=300, height=30, bg=BG_COLOR_LIGHT)
        self.spacer_bottom = Frame(self.left_frame, width=300, height=30, bg=BG_COLOR_LIGHT)

    def initialize_label(self):
        """
        Initialize all the Label widget.
        Set the text, the colors, the font and the type of widget.
        """
        self.image_label = Label(self.right_frame, image=self.image)
        self.labelText.set('Select the files')
        self.result_label = Label(self.right_frame, text="Result from the selected files :", font=HEADLINE_FONT, bg=BG_COLOR_LIGHT, fg=ACCENT_COLOR_DARK)
        self.select_files_label = Label(self.left_frame, textvariable=self.labelText, bg=BG_COLOR_LIGHT, fg=ACCENT_COLOR_DARK, font=INPUT_FONT, pady=10)
        
    def initialize_button(self):
        """
        Initialize all the Button widget.
        Set the text, the colors, the font and the type of widget.
        """
        self.render_button = Button(self.left_frame, text="Send", bg=ACCENT_COLOR_DARK, fg=TEXT_COLOR, width=25, font=INPUT_FONT, padx=2)
        self.file_query_button = Button(self.left_frame, text="Find", bg=ACCENT_COLOR_DARK, fg=TEXT_COLOR, width=25, font=INPUT_FONT, padx=2, command=self.open_files)
        
    def check_file_integrity(self):
        """
        Check the files integrity. Whether the files are corrupted or 
        not enough has been selected

        Args:
            files (Files): A list containing all the files
        """
        number_of_files = len(self.files)
        if(number_of_files != 0):
            self.number_of_files = number_of_files;
            print(self.files[0].name)
            self.media = verify_file(self.files[0].name)
            self.read_file_properties()
        else:
            messagebox.showwarning("Files error", "No files has been selected, please try again.")

    def read_file_properties(self):
        """
        Read the properties of the Media and change the ui with thoses new values
        """
        if(self.media.isMono == True):
            self.monoText.set("The file is mono")
        else:
            self.monoText.set("The file is not mono")
        if(self.media.isStereo == True):
            self.stereoText.set("The file is stereo")
        else:
            self.monoText.set("The file is not stereo")
        if(self.media.onlyAudio == True):
            self.onlySoundText.set("The file only contains audio")
        else:
            self.onlySoundText.set("The file does not only contains audio")
        if(self.media.hasVideo == True):
            self.hasVideoText.set("The file contains video")
        else: 
            self.hasVideoText.set("The file has no video")
            

    def open_files(self):
        """
        Open a dialog to select files.
        """
        self.files = fd.askopenfiles(title="Select video and audio files", filetypes=FILE_TYPES)
        self.check_file_integrity()

    def initialize_checkbox(self):
        """
        Initialize all the Checkbox widget.
        Set the text, the colors, the font and the type of widget.
        """
        self.mono_checkbox = Label(self.left_frame, textvariable=self.monoText,font=INPUT_FONT, bg=BG_COLOR_LIGHT, fg=ACCENT_COLOR_DARK)
        self.stereo_checkbox = Label(self.left_frame, textvariable=self.stereoText,font=INPUT_FONT, bg=BG_COLOR_LIGHT, fg=ACCENT_COLOR_DARK)
        self.no_video_checkbox = Label(self.left_frame, textvariable=self.hasVideoText,font=INPUT_FONT, bg=BG_COLOR_LIGHT, fg=ACCENT_COLOR_DARK)
        self.no_sound_checkbox = Label(self.left_frame, textvariable=self.onlySoundText, font=INPUT_FONT, bg=BG_COLOR_LIGHT, fg=ACCENT_COLOR_DARK)
        
    def initialize_misc(self):
        """
        Initialize all the miscellaneous widget, such as the ProgressBar
        Set the text, the colors, the font and the type of widget.
        """
        self.progress_bar = Progressbar(self.right_frame)
        
    def place_widget(self):
        """
        Place the initialize widget into the Window. Using Grid and Pack system.
        """
        self.left_frame.grid(row=0,column=0,pady=10, padx=5)
        self.right_frame.grid(row=0,column=1, pady=10, padx=10)
        self.place_left_frame_content()
        self.place_right_frame_content()
       
    def place_left_frame_content(self):
        """
        Place the initialize widget into the left frame. Using the Pack system.
        """        
        self.select_files_label.pack(fill=X)
        self.spacer_top.pack()
        self.file_query_button.pack(fill=X)
        self.spacer_middle.pack()
        self.mono_checkbox.pack(fill=X)
        self.stereo_checkbox.pack(fill=X)
        self.no_video_checkbox.pack(fill=X)
        self.no_sound_checkbox.pack(fill=X)
        self.spacer_bottom.pack()
        self.render_button.pack(fill=X)
        
    def place_right_frame_content(self):
        """
        Place the initialize widget into the right frame. Using the Pack system.
        """    
        self.result_label.pack(fill=X)
