#!/usr/bin/env python3
import argparse
import logging
import pathlib
from media import Media

def initialize_arguments():
    """
    Initialise the arguments possible for the command line execution
    """
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', nargs='?', help='Path of the media file')
    parser.add_argument('-l', '-log', action='store_true', help='Enable logging') 
    return parse_arguments(parser.parse_args())

def initialize_logger(hasLogger):
    """ Enable or disable the logging of the script.

    Args:
        hasLogger (bool): if the script should log or not.
    """
    if(hasLogger):
        logging.basicConfig(filename='app.log', filemode='w', format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.NOTSET)
        logging.info('The program has been started')
    else:
        logging.disabled = False
        

def verify_file(filename):
    """Verify if the given filename exist on the computer

    Args:
        filename (string): The filename

    Raises:
        OSError: Error explaining that the file does not exist

    Returns:
        Media: A media object of the given file
    """
    avPath = pathlib.Path(filename)
    if not avPath.is_file():
        logging.error('File: "%s" doesnt exist' %avPath)     
        raise OSError('File: "%s" doesnt exist' %avPath)
    return Media(avPath)
    
def parse_arguments(args):
    """
    Parse the given argument and initialize the logger

    Args:
        args (string): the arguments such has the filename and the -l

    Returns:
        Media: A media object of the given file
    """
    initialize_logger(args.l)
    return verify_file(args.filename)

if __name__ == '__main__':
    """
    Main function for the script.
    """
    media = initialize_arguments()
    media.show_info()