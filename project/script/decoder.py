#!/usr/bin/env python3

import logging

class Decoder():
    """
    Object encapsulating DSP processes to demodulate YaLTC track from audio file;
    Decoders are instantiated by their respective Recording object. Produces
    plots on demand for diagnostic purposes.
    """
    def __init__(self):
        self.initialize_variables()

    
    def initialize_variables(self):
        """
        Initialize the decoder variable to None.
        """
        logging.debug("Initializing decoder variables")
        self.soundDataExtracted = None
        self.cachedConvolution = {'sound_extract_position': None}
