# Code Style

*This projet is base on [PEP 8]([PEP 8 -- Style Guide for Python Code | Python.org](https://www.python.org/dev/peps/pep-0008/))* 
*All the documentations should be writtent in English only*
*Last edit `2022-01-28`*

In this file you will find all the code style rules and the good practice who's been applied to the project. Its mandatory to follow those rules in the goal to ensure the readability of the code and his life-cycle.

## Indentation

This project use **4 spaces** has indentation. 
**Do no use tabulation**

When a function with multiple lines has been called, its mandatory to delimit the arguments with **4 levels** of indentation.

```python
# Aligned with opening delimiter.
foo = long_function_name(var_one, var_two,
                         var_three, var_four)

# Add 4 spaces (an extra level of indentation) to distinguish arguments from the rest.
def long_function_name(
        var_one, var_two, var_three,
        var_four):
    print(var_one)

# Hanging indents should add a level.
foo = long_function_name(
    var_one, var_two,
    var_three, var_four)
```


If an array or a function need to be written over multiple lines, its important to align the elements at the same level of the closing bracket. 

```python
my_list = [
    1, 2, 3,
    4, 5, 6,
    ]
result = some_function_that_takes_arguments(
    'a', 'b', 'c',
    'd', 'e', 'f',
    )
```

## Line length

**A line should never exceed 79 characters**
For flowing long blocks of text with fewer structural restrictions (docstrings or comments), the line length should be limited to 72 characters.

## Imports

Imports should usually be on separate line.
Imports are always put at the top of the file, just after any module comments and docstrings, and before module globals and constants.
Imports should be grouped in the following order:

1. Standard library imports
2. Related third party imports
3. Local application/libray specific imports

```python
# Correct:
import os
import sys
from subprocess import Popen, PIPE
```

## String 

This project use single quote string only.

```python
coolString = ''
```

## Spacing

**Avoid extraneous whitespace if its immediately inside parentheses, brackets or braces**

```python
# Correct:
spam(ham[1], {eggs: 2})
# Wrong:
spam( ham[ 1 ], { eggs: 2 } )
```

**Avoid extraneous whitespace if its between a trailing comma and a following close parenthesis**

```python
# Correct:
if x == 4: print(x, y); x, y = y, x
# Wrong:
if x == 4 : print(x , y) ; x , y = y , x
```

**Avoid extraneous whitespace if its between the name of a function and its parentheses.**

**When making an value assignment, leave a space before and after the `=`**

```python
# Correct:
x = 1
y = 2
long_variable = 3

# Wrong:
x             = 1
y             = 2
long_variable = 3
```

## Comments

Comments should be complete sentences. The first word should be capitalized, unless it is an identifier that begins with a lower case letter (never alter the case of identifiers!)

Block comments generally consist of one or more paragraphs built out of complete sentences, with each sentence ending in a period.

Comments that contradict the code are worse than no comments. Always make a priority of keeping the comments up-to-date when the code changes!

Ensure that your comments are clear and easily understandable to other speakers of the language you are writing in.

Comment need to be written under the function with 4 level of indentation.

```python
def hello_world():
    # A simple comment preceding a simple print statement
    print("Hello World")
    
 def hello_long_world():
    # A very long statement that just goes	 on and on and on and on and
    # never ends until after it's reached the 80 char limit
    print("Hellooooooooooooooooooooooooooooooooooooooooooooooooooooooo World")
```

## Documentation

Documentation should be written with docstring. The documentation can be single line or multiline. 
The documentation need to be under the class or function with 4 levels of indentation. 

```python
class Vehicles(object):
    '''
    The Vehicle object contains a lot of vehicles

    Args:
        arg (str): The arg is used for...
        *args: The variable arguments are used for...
        **kwargs: The keyword arguments are used for...

    Attributes:
        arg (str): This is where we store arg,
    '''
    def __init__(self, arg, *args, **kwargs):
        self.arg = arg

    def cars(self, distance,destination):
        '''We can't travel distance in vehicles without fuels, so here is the fuels

        Args:
            distance (int): The amount of distance traveled
            destination (bool): Should the fuels refilled to cover the distance?

        Raises:
            RuntimeError: Out of fuel

        Returns:
            cars: A car mileage
        '''
        pass
```



## Naming Conventions

**A variable should be short and precise. The variable must no be an abbreviation and contain number.** 

```python
class CamelCase # Class names should normally use the CapWords convention.
module_or_package # Modules and packages should have short, all-lowercase names. Underscores can be used in the module name if it improves readability.
def ma_function() # mixedCase 
CONST # UPPER_CASE_WITH_UNDERSCORES
```
