# Tic Tac Sync

## Description

**Disclaimer : ** This project is still a work in progress.

TicTacSync is made out of a python module and a python tkinter ui app

## Installation

From the official author :

```bash
git clone git@git.sr.ht:~proflutz/yaltc
```

For this specific fork :
```bash
git clone https://gitlab.com/Xeudo/tictacsync
```


## Dependencies

At the moment this application has those dependency.
Make sure to add FFMPEG and TheFuzz to the executable path

1. Python 3.10 : [Python Release Python 3.10.0 | Python.org](https://www.python.org/downloads/release/python-3100/)
2. FFMPEG : [Video showing how to install  FFMPEG ](https://www.youtube.com/watch?v=wXDsMKJyK9c&ab_channel=YourTechRemote)
2. TheFuzz : `pip install thefuzz` 

## Usage

To use the application with the UI. Go to  `project/dist/main.exe` and execute the .exe

To use the application from the command line go in the folder `project/script` and run the following command :

```bash
python script.py [name of file] [-l with or without log]
python script.py canon.wav -l
```


To execute the tests. Go to `project/script` and run the following command : 

```shell
python -m unittest media_test
```
