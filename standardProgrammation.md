# Standard de programmation

*Ce projet est basé sur le standard de programmation [PEP 8]([PEP 8 -- Style Guide for Python Code | Python.org](https://www.python.org/dev/peps/pep-0008/))* 
*Ce projet doit être codé et documenté en anglais.*
*Dernière modification `2022-01-28`

Dans ce fichier vous retrouverez le standard de programmation ainsi que les bonnes pratiques qui sont appliquées dans le projet. Il est impératif de le suivre afin d'assurer une bonne lisibilité du code ainsi que sa maintenance. En cas de doute, contacter les responsables du projet. 

## Indentation

Le projet utilise **4 espaces** pour faire les indentations.

Lorsqu'une fonction est appelée et qu'elle comporte plusieurs arguments sur plus d'une ligne, il est important de les délimiter à l'aide de 4 niveaux d’indentation.

```python
# Aligned with opening delimiter.
foo = long_function_name(var_one, var_two,
                         var_three, var_four)

# Add 4 spaces (an extra level of indentation) to distinguish arguments from the rest.
def long_function_name(
        var_one, var_two, var_three,
        var_four):
    print(var_one)

# Hanging indents should add a level.
foo = long_function_name(
    var_one, var_two,
    var_three, var_four)
```

Si un tableau ou une méthode doit se trouver sur plus d'une ligne, il est important d'aligner les éléments et la balise fermente au même niveau indentation.

```python
my_list = [
    1, 2, 3,
    4, 5, 6,
    ]
result = some_function_that_takes_arguments(
    'a', 'b', 'c',
    'd', 'e', 'f',
    )
```

## Taille maximum d'une ligne

**Une ligne ne doit pas dépasser 79 caractères.**

Cependant pour la documentation ou des commentaires il est préférable de couper à partir de 72 caractères

## Importations

Les importations doivent être sur une ligne chaque.  Cependant si plusieurs importations proviennent de la même dépendance ils doivent être sur la même ligne. Les importations doivent être en haut du fichier. 

```python
# Correct:
import os
import sys
from subprocess import Popen, PIPE
```

## String 

Ce projet utilise des apostrophes simples pour un string.

```python
coolString = ''
```

## Espacement 

Éviter les espaces inutiles dans entre des `parenthèses`, `tableau` ou `bracket`.

```python
# Correct:
spam(ham[1], {eggs: 2})
# Wrong:
spam( ham[ 1 ], { eggs: 2 } )
```

Éviter d'entouré une `virgule`, `point-virgule`, `double-point` d'espacement. Mettre une espace seulement après. 

```python
# Correct:
if x == 4: print(x, y); x, y = y, x
# Wrong:
if x == 4 : print(x , y) ; x , y = y , x
```

Mettre les parenthèses d'une méthode directement après son nom. Ne pas mettre d'espace entre le nom et les parenthèses. 

 Lors d’assignation de variable, laisser un espace avant et après le `=` 

```python
# Correct:
x = 1
y = 2
long_variable = 3

# Wrong:
x             = 1
y             = 2
long_variable = 3
```

## Commentaire 

Un commentaire doit commencer par une majuscule sauf dans le cas ou c'est un identifiant. 
Un commentaire de bloque doit être composé de phrases qui se termine par un point.
Un commentaire doit être écrit en anglais. Un commentaire doit être clair et précis. Ne pas mettre plus de détails qu'il faut. 
Un commentaire doit être écrit sous la méthode avec 4 niveaux d'indentations.

## Documentation

La documentation doit être faite avec **Docs string**. Selon le cas, il est possible que la documentation soit sur une ligne ou plusieurs lignes.
La documentation doit se trouver sous la méthode ou la classe avec 4 niveaux d'indentations.

```python
class Vehicles(object):
    '''
    The Vehicle object contains a lot of vehicles

    Args:
        arg (str): The arg is used for...
        *args: The variable arguments are used for...
        **kwargs: The keyword arguments are used for...

    Attributes:
        arg (str): This is where we store arg,
    '''
    def __init__(self, arg, *args, **kwargs):
        self.arg = arg

    def cars(self, distance,destination):
        '''We can't travel distance in vehicles without fuels, so here is the fuels

        Args:
            distance (int): The amount of distance traveled
            destination (bool): Should the fuels refilled to cover the distance?

        Raises:
            RuntimeError: Out of fuel

        Returns:
            cars: A car mileage
        '''
        pass
```



## Nomenclature

**Le nom d'une variable doit être court et précis. Éviter les abréviations ainsi que les nombres à l'intérieur.** 

```python
class CamelCase # Class names should normally use the CapWords convention.
module_or_package # Modules and packages should have short, all-lowercase names. Underscores can be used in the module name if it improves readability.
def ma_function() # mixedCase 
CONST # UPPER_CASE_WITH_UNDERSCORES
```
